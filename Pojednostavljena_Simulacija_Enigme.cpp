#include <iostream>
#include <string>
#include <array>
using namespace std;

int main(){
	
	int poz,reset=26;
	char k,a;
	string p;
	bool t = 1;
	
	do{
		system("cls");
	
		cout<<"~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"<<endl;
		cout<<"|U programu koristite mala slova i za razmak koristite'_'|"<<endl;
		cout<<"~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"<<endl<<endl;
	
	
		cout<<"Za kodiranje unesite 'k' a za dekodiranje unesite 'd': ";
		cin>>k;
	
		switch (k){
		
			case 'k':
			
				cout<<"Unesite poziciju rotora (1-25): ";
				cin>>poz;
				cout<<"Unesite sto zelite kodirati: ";
				cin>>p;
				for(int i=0;i<p.length();i++){
				
					if(p[i]!=95){
						p[i]+=poz;
						if(p[i]>122 || p[i]<97){
							p[i]=p[i]-reset;
						}
					}
				}
			
				cout<<"Vasa kodirana poruka je: "<<p<<endl;
			
				break;
			
			case 'd':
				
				cout<<"Unesite poziciju rotora koju ste koristili u kodriranju(1-25): ";
				cin>>poz;
				cout<<"Unesite sto zelite dekodirati: ";
				cin>>p;
				for(int i=0;i<p.length();i++){
					
					if(p[i]!=95){
						p[i]-=poz;
						if(p[i]>122 || p[i]<97){
							p[i]+=reset;
						}
					}
				}
				
				cout<<"Vasa dekodirana poruka je: "<<p<<endl;
				
				break;
				
			default:
				cout<<"Krivi unos.";
				break;
		}
		cout<<"Zelite li nastaviti koristiti program(y/n)"<<endl;
		cin>>a;
		
		if(a=='n')
			t=0;
			
	}while(t!=0);
	
	return (0);
}
