**Sveučilište Jurja Dobrile**

**Tehnički fakultet u Puli**

**Boris Vujica**

**Iva Krišto**

**Mihael Holi**

**David Barna**

**Tin Nanndo Jovanović**

Engima machine

**Seminarski rad**

**Studijski smjer: računarstvo**

**Kolegij: Logika i diskretna matematika**

**Mentor: doc. dr. sc. Siniša Miličić**

**Sadržaj:**

[**1.Uvod [2](#uvod)**](#uvod)

[**1.1. Izgled I Konstrukcija Enigme
[3](#izgled-i-konstrukcija-enigme)**](#izgled-i-konstrukcija-enigme)

[**1.1.1. Tipkovnica i zaslon
[3](#tipkovnica-i-zaslon)**](#tipkovnica-i-zaslon)

[**1.1.2. Razvodna ploča [4](#razvodna-ploča)**](#razvodna-ploča)

[**1.1.3. Premetača jedinica
[4](#premetača-jedinica)**](#premetača-jedinica)

[**1.2. Princip rada Enigme
[6](#princip-rada-enigme)**](#princip-rada-enigme)

[**1.3. Proces šifriranja i dešifriranja
[8](#proces-šifriranja-i-dešifriranja)**](#proces-šifriranja-i-dešifriranja)

[**1.3.1. Način šifriranja i dešifriranja poruke
[10](#način-šifriranja-i-dešifriranja-poruke)**](#način-šifriranja-i-dešifriranja-poruke)

**Popis slika:**

**Slika.1: Enigma uređaj**

**Slika.2: Tipkovnica**

**Slika.3: Razvodna ploča**

**Slika.4: Premetača jedinica**

**Slika.5: Enkripcija zaslona**

**Slika.6: Enigma tokom Drugog Svjetskog rata**

**Slika.7: Matematička formula enigme**

**Slika.8: Enkripcijska funkcija**

# **1.Uvod**

Naglim razvojem elektrotehnike i završetkom Prvog svjetskog rata
početkom 20. stoljeća javila se potreba za razvojem učinkovitog i
sigurnog sustava razmjene informacija. Zbog svojstva širenja radio
valova u korištenom frekvencijskom području, prijenose s vojne radio
opreme tijekom Prvog svjetskog rata mogle su primati sve zaraćene
strane. Vojni stručnjaci, nezadovoljni ovom nerazvijenom
radiokomunikacijskom tehnologijom, zahtijevali su uvođenje novijih,
sigurnijih kriptiranih sustava zaštite podataka koji bi im omogućili
sigurnu komunikaciju s udaljenim vojnim postrojbama bez opasnosti da
neprijatelji saznaju informaciju. Ovo je počelo zamjenjivati stare
načine razmjene podataka s novijim, učinkovitijim Enigma uređajima.

## **1.1. Izgled I Konstrukcija Enigme**

Enigma na prvi pogled podsjeća na običnu pisaću mašinu, ali ona je puno
važnija. Enigma je elektromehanički prijenosni uređaj za mehaničko
šifriranje i dešifriranje teksta. Prva Enigma je bila spakirana u
kompaktnoj kutiji dimenzija 35 × 28 × 40 centimetara, a težila je oko 12
kilograma. Napajala se pomoću ugrađene baterije, a sastojala se od
četiri osnovna elementa povezana žicama:

1\) zaslon

2\) tipkovnica

3\) premetača jedinica

4\) razvodna ploča

![](https://static.nationalgeographic.rs/Pictures/950/1689/16147/jpeg/cuvena_masina_enigma_prodata_za_233_000_dolara_241285526){width="2.611111111111111in"
height="1.9583333333333333in"}

**Slika.1 Enigma uređaj**

### **1.1.1. Tipkovnica i zaslon**

Enigma tipkovnica se sastoji od 26 tipki sa slovima latinične abecede.
Ne sadrži interpunkcijske znakove, brojeve ili funkcijske tipke. Umjesto
tipki za ispis teksta, Enigma ima ekran sa 26 prozorčića ispod kojih se
nalaze žarulje. Prozorčići na ekranu označeni su sa 26 latiničnih slova,
kao na tipkovnici. Tipkovnica se koristi za unos javnog ili šifriranog
teksta. Ono što žaruljica zaslona prikazuje je slovo šifriranog teksta,
odnosno izlaz šifriranog teksta koji se dobije svaki put kada se
pritisne tipka na tipkovnici.\
**Slika 2.tipkovnica**

![](https://s.yimg.com/ny/api/res/1.2/DTpmJplqoWymJ2e2zLTjIw--/YXBwaWQ9aGlnaGxhbmRlcjt3PTk2MDtoPTU3MDtjZj13ZWJw/https://s.yimg.com/os/en-US/blogs/en-us-visit-britain-travel/Enigma.jpg){width="2.8562160979877516in"
height="1.814887357830271in"}

#### **1.1.2. Razvodna ploča**

Enigma je opremljena fiksnim ulaznim prstenom koji predstavlja vezu
između centrale i prvog čistača. Razvodna ploča nalazi se ispred
tipkovnice i sastoji se od ploče na kojoj su ispisana slova abecede,
svaka pored svog ulaza, i šest (ili više) pari utikača povezanih žicama
koje mogu zamijeniti 12 od 26 slova (ili više). Naime, centrala povezuje
jedno slovo s drugim, što također znači da je i drugo slovo spojeno
natrag na prvo slovo, tj. mapiranje je obavljeno. Na primjer, ako
spojimo slovo A sa slovom S na centrali, to znači da će se slovo A
tijekom enkripcije preslikati u slovo SI (S u A) na suprotan način.

**Slika 3. Razvodna ploča sa slovima**

![](https://www.math.arizona.edu/~dsl/images/enigma19.gif){width="2.645138888888889in"
height="1.5729166666666667in"}

##### **1.1.3. Premetača jedinica**

Glavna komponenta Enigme je premetača jedinica. Sastoji se od 3
mehaničke miješalice (rotora) i fiksnog reflektora. Žice unutar stroja
počinju od tipkovnice na 26 i idu u sklopnu jedinicu, gdje se odvija
proces šifriranja aktiviranjem rotora. Svi rotori su na istoj osovini.
Svaki rotor je disk promjera 10 cm izrađen od bakelita ili tvrde gume.
Svaki rotor ima 26 fiksnih kontakata kružno raspoređenih po obodu sa
svake strane. Unutar svakog rotora nalazi se 26 žica, tako da se svaki
kontakt na jednoj strani diska povezuje s drugim kontaktom na drugoj
strani. Kada su rotori postavljeni jedan pored drugog na istoj osovini,
klinovi jednog rotora naliježu na kontakte susjednog rotora.

**[Zanimljivost 1:]{.underline}** Svaki rotor koji se nalazi u Enigmi
može se postaviti na jedan od 26 mogućih položaja. Može se pomicati
ručno pomoću žljebastog kotača, a kada je poklopac postavljen, iskoči iz
unutrašnjosti Enigme. Operater zna gdje su rotori jer svaki ima prsten
sa slovima izvana. Kroz prozorčić na poklopcu Enigme vidljivo je slovo
na prstenu koje označava rotacijski položaj rotora.

**[Zanimljivost 2:]{.underline}** Većina modela Enigma sastojala se od
tri rotora, ali s vremenom je većina Enigmi vojske i zrakoplovstva imala
četiri ili čak pet, a njemačka mornarica imala je čak 6-8 rotora.

Izlaz trećeg rotora spojen je na reflektor - statički mehanički disk s
međusobno povezanim električnim kontaktima spojenim u parovima s jedne
strane. Njegova zadaća je da električni signal šalje natrag kroz rotor,
ali drugačijim putem.

**Slika 4. Premetača jedinica**

![](https://content.instructables.com/FJM/6UR0/GJQEBFP7/FJM6UR0GJQEBFP7.jpg?auto=webp&frame=1&fit=bounds&md=50b1e11bc2227ae7fd4720fc03dd3a18){width="2.4849398512685914in" height="1.875in"}

# **1.2. Princip rada Enigme**

Pritiskom na bilo koju tipku tipkovnice šaljemo električni signal od
tipkovnice, putem ugrađenih žica do premetače jedinice zatim započinjemo
proces šifriranja, te onda zasvijetli lampica ispod jednog od slova na
zaslonu. Osvjetljeno je slovo uvijek drugačije od onog koje se nalazi na
tipki koju pritisnemo. U suprotnom, ako pritisnemo tipku sa slovom koje
je osvijetljeno (a prije toga premetala postavimo u odgovarajući početni
položaj), to slovo će se pojaviti obasjano na zaslonu - time je obavljen
proces dešifriranja**.**

Premetala imaju glavnu ulogu u procesu šifriranja i dešifriranja.
Pritisak tipke uzrokuje rotaciju prvog premetala za 1/26 punog okreta.
Kada prvo premetalo napravi puni okret, tada će drugo premetalo se
pomaknuti za jedno mjesto, odnosno za jednu 1/26 punog okreta. Drugo se
premetalo neće ponovno pomaknuti sve dok prvo premetalo ponovno ne
napravi puni zaokret, a za to je potrebno sljedećih 25 enkripcija. Kada
drugo premetalo napravi puni okret pomiče se treće premetalo za jedno
mjesto. Zaključak je da sva tri premetala zajedno mogu zauzeti 26 · 26 ·
26 = 17576 položaja.

Uočili smo da je reflektor uređaja sličan premetalu, ali zbog
električnih kontakata sa samo jedne strane on je statičan uređaj i ne
povećava broj položaja premetala. Na prvi pogled možemo pomisliti da je
reflektor nepotreban dodatak stroju, ali njegova je uloga važna u
dešifriranju i šifriranju poruka.

**Slika 5. Enkripcija zaslona**

![](https://cdni0.trtworld.com/w960/h540/q75/8749-trtworld-399671-438795.jpg){width="2.9583333333333335in"
height="1.9722222222222223in"}**\
**

# **1.3. Proces šifriranja i dešifriranja**

Kada upišemo neki tekst, na zaslonu Enigme dobivamo šifrirani tekst, te
su tako njemački službenici slali tajne poruke udaljenim vojnim
postrojbama u Drugom svjetskom ratu.

Prije nego su započeli enkripciju, premetala su postavili u neki od
mogućih početnih položaja. Premetala se mogu postaviti na 17 756 načina,
te je toliko i mogućih početnih položaja za njih. No, na broj početnih
položaja utječe i kako su ona postavljena.Broj mogućih početnih položaja
je se poveća za šest puta jer se tri premetala mogu postaviti na šest
različitih načina Takvo početno postavljanje premetala, odredit će i
kako će se enkriptirati poruka. Osim premetala, moguće je postaviti
premetaču ploču na određeni način.

Premetača ploča Enigme ima šest kablova koji omogućuju zamjenu mjesta
šest parova slova. Slova kojima treba zamijeniti mjesta isto tako
povećavaju broj svih mogućih položaja i postavki koje je potrebno
postaviti na stroju. Knjiga šifri je diktirala početne položaje prilikom
šifriranja povjerljivih podataka. U njoj su nabrojeni položaji premetala
i razvodne ploče za svaki pojedini dan. Ona je dostupna svima unutar
komunikacijske mreže. Njemački službenici, na Enigmi, svaki su mjesec
dobivali novu knjigu šifri koja je određivala koji će se ključ koji dan
koristiti.

![](https://www.pressetext.com/nfs/187/400/lowres/lowres.jpg){width="3.086065179352581in"
height="1.9609372265966754in"}

**Slika 6. Korištenje enigme tokom Drugog Svjetskog rata**

**\
**

## **1.3.1. Način šifriranja i dešifriranja poruke**

|                                                                        |
| ---------------------------------------------------------------------- |
| 1\. Spojevi na razvodnoj ploči: B/D -- C/K -- Q/Z -- M/T -- F/W -- S/P |
| 2\. Redoslijed premetala: III -- I -- II                               |
| 3\. Orijentacija premetala: A -- E -- L                                |

**Tablica.1 Primjer dnevnog ključa**

**Šta operater mora učiniti da unese dnevni ključ:**

**1.** Zamijeniti slovo B na razvodnoj ploči sa slovom D, tj. povezati
ih kablom, te nakon toga zamijeniti slovo C slovom K i onda nastaviti
sve dok se ne dogodi posljednja zamjena slova S sa slovom P.

**2.** Na osovini ostaviti treće premetalo**,** prvo na drugo mjesto i
drugo na treće mjesto.

**3.** Zato što svako premetalo na svom obodu ima urezana slova abecede,
operater ih može postaviti u određeni položaj. Prvo premetalo treba
postaviti tako da na njemu bude vidljivo slovo A, drugo premetalo treba
postaviti tako da na njemu bude vidljivo slovo E, a treće tako da na
njemu bude vidljivo slovo L. Nakon što operater postavi premetala na
prave pozicije, započinje se postupak šifriranja, odnosno započinje se s
unosom otvorenog teksta. Šifrirani tekst su slova koja se pojavljuju
osvjetljena na zaslonu. Kada su njemački operateri završili sa
šifriranjem, tekst su predali radio operateru koji ga zatim šalje
primatelju radiovalovima. Zrcalni postupci su kada primatelj namjesti
stroj prema dnevnoj šifri a na zaslonu stroja dobiva osvjetljena slova
otvorenog teksta.

Razmatrajući varijantu s tri rotora, neka P predstavlja transformaciju
prespojene ploče, U transformaciju reflektora, a L, M i R transformacije
prvog, drugog i trećeg rotora. Tada šifrirano E se može izraziti kao

![](http://e.math.hr/old/enigma/Enigma_files/equation5.png){width="2.4166666666666665in"
height="0.5104166666666666in"}

**Slika.7 Matematička formula funkcije engime**

Nakon svakog pritiska na tipku transformacija se mijenja okretanjem
rotora. Ukoliko se prvi rotor R okrene za i mjesta, transformacija
postaje ***p^i^Rp^-i^***, gdje je ***p*** ciklička permutacija koja
preslikava A u B, B u C itd. Također, transformacije drugog i trećeg
rotora mogu se prikazati kao j i k okreta rotora M i L. Naposljetku,
enkripcijska funkcija može se prikazati kao:

![](http://e.math.hr/old/enigma/Enigma_files/equation8.png){width="5.0in" height="0.3645833333333333in"}

**Slika. 8 Enkripcijska funkcija**

**1.4 LITERATURA:**

<https://brilliant.org/wiki/enigma-machine/>

<http://e.math.hr/old/enigma/index.html>
